let mongoose = require('mongoose');

const taskSchema = new mongoose.Schema(
	{
		firstName: String,
		lastName: String,
		userName: String,
		password: String
	}
);

module.exports = mongoose.model("User", taskSchema);