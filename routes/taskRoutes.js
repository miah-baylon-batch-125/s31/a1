let express = require('express');
let router = express.Router();

//models
let User = require('./../models/User');

//controllers
let userController = require('./../controllers/userControllers')


/*router.post("/addTask", (req, res)=>{

	let requestBodyData = req.body
	res.send(req.body);
})


router.get("/tasks", (req,res) => {
	res.send(`This is comming from the task route`)
})*/

//insert a new user

router.post("/add-task", (req, res) => {
	//console.log(req.body);
/*
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		userName: req.body.userName,
		password: req.body.password
	})

	newUser.save( (savedUser, error) => {
		if (error) {
			res.send(error)
		} else {
			res.send(`New user saved:`, savedUser);
		}
	})*/

	userController.createUser(req.body).then( (result) => {
		res.send(result)
	})

})

//retrieve all user


router.get("/users", (req, res) => {
	userController.getAll().then(result => res.send(result));

	})

/*router.get("/users", (req, res) => {
	//model.method
		//search users from the database
	//send it as a response

	User.find({}, (result, error)=> {
		if(error){
			res.send(error)
		} else {
			//send it as response
			res.send(`Users:`, result)
		}
	})

});*/

//retrieve a specific user
	/*router.get("/users/:id", (req, res)=> {
		//console.log(req.params);

		let params = req.params.id
		//model.method
			User.findById(params, (result, error)=> {
				if(error) {
					res.send(error)
				} else {
					//send document as a response
					res.send(result)
				}
		 })
		
	})*/

//retrieve a specific user

router.get("/users/:id", (req, res) => {
	let params = req.params.id

	userController.getUser(params).then(result => res.send(result));


	})

//update users' info

/*router.put("/users/:id", (req, res) => {
	let params = req.params.id
	//model.method
	User.findByIdAndUpdate(params, req.body, {new: true}, (result, error) => {
		if(error){
			res.send(error);
		} else {
				//send update
			res.send(result);
		}
	})


})*/

//update user info
	router.put("/users/:id", (req, res) => {
		let params = req.params.id

	userController.updateUser(params, req.body).then(result => res.send(result))
	});


//delete user 
/*router.delete("/users/:id", (req, res) => {
	let params = req.params.id
	//model.method
	User.findByIdAndDelete(params, (error, result) => {
		if(error){
			res.send(error)
		} else {
			//send a response true if deleted
			res.send(true)
		}
	})

	
})
*/

//delete user

router.delete("/users/:id", (req, res) => {
	let params = req.params.id
	userController.deleteUser(params).then(result => res.send (result))
});



module.exports = router;