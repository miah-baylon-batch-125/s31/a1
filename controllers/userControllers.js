const User = require('./../models/User');

//insert a new user
module.exports.createUser = function(reqBody){
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		userName: reqBody.userName,
		password: reqBody.password
	});

	return newUser.save().then( (savedUser, error) => {
		if(error){
			return error
		} else {
			return savedUser
		}
	});
}

//createUser(req.body)


//retrieve user
module.exports.getAll = () => {
	//Model.method
	return User.find().then( (result, error) => {
		if(error){
			return error
		} else {
			//send [ array of objects] as a response
			return result
		}
	})

	
};

//getAll();

//retrieve spesicfic user

module.exports.getUser = (params) => {
	//Model.method
 return	User.findById(params).then((result) => {
		//send user document as a return
 	return result
 })

}


//update user

module.exports.updateUser = (params, reqBody) => {
	//model method
	return User.findByIdAndUpdate(params, reqBody, {new: true}).then( (result, error) => {
		if (error) {
			return error
		} else {
			return result
		}
	})

		//return user object


	}

//delete user
module.exports.deleteUser = (params) => {

	return User.findByIdAndDelete(params).then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}