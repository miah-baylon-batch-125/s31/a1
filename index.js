let express = require('express');
let app = express();

let mongoose = require('mongoose');
const PORT = 5000;

//router
let tasksRoutes = require('./routes/taskRoutes')

app.use(express.json());
app.use(express.urlencoded({extended:true}));
mongoose.connect("mongodb+srv://ppr0119:admin@cluster0.pzreh.mongodb.net/s31?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
).then(()=> console.log(`Connected to Database!`))
.catch((error)=>console.log(error));

app.use("/", tasksRoutes);

app.listen(PORT, () => console.log (`Server running on port ${PORT}`));